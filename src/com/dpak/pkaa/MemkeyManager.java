package com.dpak.pkaa;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.dpak.pkaa.bean.PubkeyBean;
import com.dpak.pkaa.util.PubkeyDatabase;
import com.dpak.pkaa.util.PubkeyUtils;

public class MemkeyManager {
	public final static String TAG = "PubkeyAuthAgent.MemkeyManager";
	
	public Map<String, KeyHolder> loadedKeypairs = new HashMap<String, KeyHolder>();

	public Resources res;

	public PubkeyDatabase pubkeydb;

	private Timer pubkeyTimer;

	private boolean savingKeys;
	
	public static class KeyHolder {
		public PubkeyBean bean;
		public Object trileadKey;
		public byte[] openSSHPubkey;
	}

	/*Don't need this
	public class MemkeyBinder extends Binder {

	}*/

	MemkeyManager(Context ctx) {
		//res = getResources();

		pubkeyTimer = new Timer("pubkeyTimer", true);

		pubkeydb = new PubkeyDatabase(ctx);
		res = ctx.getResources();

		// load all marked pubkeys into memory
		List<PubkeyBean> pubkeys = pubkeydb.getAllStartPubkeys();

		for (PubkeyBean pubkey : pubkeys) {
			try {
				PrivateKey privKey = PubkeyUtils.decodePrivate(pubkey.getPrivateKey(), pubkey.getType());
				PublicKey pubKey = PubkeyUtils.decodePublic(pubkey.getPublicKey(), pubkey.getType());
				Object trileadKey = PubkeyUtils.convertToTrilead(privKey, pubKey);

				addKey(pubkey, trileadKey);
			} catch (Exception e) {
				Log.d(TAG, String.format("Problem adding key '%s' to in-memory cache", pubkey.getNickname()), e);
			}
		}
	}
	
	public boolean isKeyLoaded(String nickname) {
		return loadedKeypairs.containsKey(nickname);
	}

	public void addKey(PubkeyBean pubkey, Object trileadKey) {
		addKey(pubkey, trileadKey, false);
	}

	public void addKey(PubkeyBean pubkey, Object trileadKey, boolean force) {
		if (!savingKeys && !force)
			return;

		removeKey(pubkey.getNickname());

		byte[] sshPubKey = PubkeyUtils.extractOpenSSHPublic(trileadKey);

		KeyHolder keyHolder = new KeyHolder();
		keyHolder.bean = pubkey;
		keyHolder.trileadKey = trileadKey;
		keyHolder.openSSHPubkey = sshPubKey;

		loadedKeypairs.put(pubkey.getNickname(), keyHolder);

		if (pubkey.getLifetime() > 0) {
			final String nickname = pubkey.getNickname();
			pubkeyTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					Log.d(TAG, "Unloading from memory key: " + nickname);
					removeKey(nickname);
				}
			}, pubkey.getLifetime() * 1000);
		}

		Log.d(TAG, String.format("Added key '%s' to in-memory cache", pubkey.getNickname()));
	}

	public boolean removeKey(String nickname) {
		Log.d(TAG, String.format("Removed key '%s' to in-memory cache", nickname));
		return loadedKeypairs.remove(nickname) != null;
	}

	public boolean removeKey(byte[] publicKey) {
		String nickname = null;
		for (Entry<String,KeyHolder> entry : loadedKeypairs.entrySet()) {
			if (Arrays.equals(entry.getValue().openSSHPubkey, publicKey)) {
				nickname = entry.getKey();
				break;
			}
		}

		if (nickname != null) {
			Log.d(TAG, String.format("Removed key '%s' to in-memory cache", nickname));
			return removeKey(nickname);
		} else
			return false;
	}

	public Object getKey(String nickname) {
		if (loadedKeypairs.containsKey(nickname)) {
			KeyHolder keyHolder = loadedKeypairs.get(nickname);
			return keyHolder.trileadKey;
		} else
			return null;
	}

	public Object getKey(byte[] publicKey) {
		for (KeyHolder keyHolder : loadedKeypairs.values()) {
			if (Arrays.equals(keyHolder.openSSHPubkey, publicKey))
				return keyHolder.trileadKey;
		}
		return null;
	}

	public String getKeyNickname(byte[] publicKey) {
		for (Entry<String,KeyHolder> entry : loadedKeypairs.entrySet()) {
			if (Arrays.equals(entry.getValue().openSSHPubkey, publicKey))
				return entry.getKey();
		}
		return null;
	}
}
