package com.dpak.pkaa;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.dpak.pkaa.bean.PubkeyBean;
import com.dpak.pkaa.util.PromptHelper;
import com.trilead.ssh2.signature.DSAPrivateKey;
import com.trilead.ssh2.signature.DSAPublicKey;
import com.trilead.ssh2.signature.DSASHA1Verify;
import com.trilead.ssh2.signature.RSAPrivateKey;
import com.trilead.ssh2.signature.RSAPublicKey;
import com.trilead.ssh2.signature.RSASHA1Verify;

public class AuthAgentOps {
	private MemkeyManager manager;
	public PromptHelper promptHelper;
	private String agentLockPassphrase;
	
	AuthAgentOps(MemkeyManager mgr) {
		manager = mgr;
	}
	
	public Map<String,byte[]> retrieveIdentities() {
		Map<String,byte[]> pubKeys = new HashMap<String,byte[]>(manager.loadedKeypairs.size());

		for (Entry<String, MemkeyManager.KeyHolder> entry : manager.loadedKeypairs.entrySet()) {
			Object trileadKey = entry.getValue().trileadKey;

			try {
				if (trileadKey instanceof RSAPrivateKey) {
					RSAPublicKey pubkey = ((RSAPrivateKey) trileadKey).getPublicKey();
					pubKeys.put(entry.getKey(), RSASHA1Verify.encodeSSHRSAPublicKey(pubkey));
				} else if (trileadKey instanceof DSAPrivateKey) {
					DSAPublicKey pubkey = ((DSAPrivateKey) trileadKey).getPublicKey();
					pubKeys.put(entry.getKey(), DSASHA1Verify.encodeSSHDSAPublicKey(pubkey));
				} else
					continue;
			} catch (IOException e) {
				continue;
			}
		}

		return pubKeys;
	}

	public boolean addIdentity(Object key, String comment, boolean confirmUse, int lifetime) {
		PubkeyBean pubkey = new PubkeyBean();
		//pubkey.setType(PubkeyDatabase.KEY_TYPE_IMPORTED);
		pubkey.setNickname(comment);
		pubkey.setConfirmUse(confirmUse);
		pubkey.setLifetime(lifetime);
		manager.addKey(pubkey, key);
		return true;
	}

	public boolean removeAllIdentities() {
		manager.loadedKeypairs.clear();
		return true;
	}

	public boolean removeIdentity(byte[] publicKey) {
		return manager.removeKey(publicKey);
	}

	public boolean isAgentLocked() {
		return agentLockPassphrase != null;
	}

	public boolean requestAgentUnlock(String unlockPassphrase) {
		if (agentLockPassphrase == null)
			return false;

		if (agentLockPassphrase.equals(unlockPassphrase))
			agentLockPassphrase = null;

		return agentLockPassphrase == null;
	}

	public boolean setAgentLock(String lockPassphrase) {
		if (agentLockPassphrase != null)
			return false;

		agentLockPassphrase = lockPassphrase;
		return true;
	}

	public Object getPrivateKey(byte[] publicKey) {
		String nickname = manager.getKeyNickname(publicKey);

		if (nickname == null)
			return null;

		if (manager.loadedKeypairs.get(nickname).bean.isConfirmUse()) {
			if (!promptForPubkeyUse(nickname))
				return null;
		}
		return manager.getKey(nickname);
	}

	private boolean promptForPubkeyUse(String nickname) {
		Boolean result = promptHelper.requestBooleanPrompt(null,
				manager.res.getString(R.string.prompt_allow_agent_to_use_key,
						nickname));
		return result;
	}
}
